/*
Josh Cristol
Kenneth Lee
Michael Rust
EE422C - 16010
 */
package assignment5;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class A5Driver
{
   public static void main(String[] args)
   {
      try
      {
         StopWatch programClock = new StopWatch();
         programClock.start();
         ArrayList<Double> timeAnalysisList = new ArrayList<Double>();
         BufferedReader input = new BufferedReader(new FileReader("assn5data.txt"));
         String line = input.readLine();
         while (line != null)
         {
            WordLadderSolver solution = new WordLadderSolver();
            String firstWord = line.substring(0, 5);
            String secondWord = line.substring(5);
            int i = 0;
            while (secondWord.charAt(i) == ' ')
            {
               secondWord = secondWord.substring(i + 1);
            }
            try
            {
               System.out.println("Word Ladder for " + firstWord + " and " + secondWord + " is:");
               System.out.println(solution.computeLadder(firstWord, secondWord));
               timeAnalysisList.add(solution.getTime());
            } catch (NoSuchLadderException e)
            {
               System.out.println(e.getMessage());
            }
            line = input.readLine();
         }
         System.out.print("\n\n\n");
         double average = 0.0;
         for (int i = 0; i < timeAnalysisList.size(); i++)
         {
            average += timeAnalysisList.get(i);
            System.out.println("The time for the " + (i + 1) + "st ladder was: " + timeAnalysisList.get(i) + " NanoSeconds or " + (timeAnalysisList.get(i) * 1E-9) + " Seconds");
         }
         average /= timeAnalysisList.size();
         System.out.println("Average time for a ladder: " + average + " NanoSeconds or " + (average * 1E-9) + " Seconds");
         programClock.stop();
         System.out.println("Total program execution time: " + programClock.getElapsedTime() + " NanoSeconds or " + (programClock.getElapsedTime() * 1E-9) + " Seconds");
      } catch (FileNotFoundException e)
      {
         System.out.println("The correct file is missing");
      } catch (IOException e)
      {
         System.out.println("Problem with input from the file");
      }
   }
}
