/*
Josh Cristol
Kenneth Lee
Michael Rust
EE422C - 16010
 */
package assignment5;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;

/**
 * Created by Josh on 4/14/2015.
 * Dictionary of words stored as a hashset for easy access
 * The dictionary is formed by reading in words from an input file
 */
public class Dictionary
{
   private static HashSet<String> dictionary;

   public Dictionary()
   {
      dictionary = new HashSet<String>();
      try
      {
         BufferedReader readDatFile = new BufferedReader(new FileReader("assn5words.dat"));
         String line = readDatFile.readLine();
         while (line != null)
         {
            if (line.charAt(0) != '*')
            {
               dictionary.add(line.substring(0, 5));
            }
            line = readDatFile.readLine();
         }
      } catch (IOException e)
      {
         System.out.println("The file you are looking for doesn't exist!!!");
      }
   }

   /**
    * checks the dictionary to see if it contains a specified word
    *
    * @param word to search for in dictionary
    * @return true or false if word is found/not found
    * (precondition: dictionary has words in it)
    */
   public boolean checkDictionary(String word)
   {
      return dictionary.contains(word);
   }
}
