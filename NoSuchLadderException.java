package assignment5;

/**
 * @author Prof. Krasner
 *         NoSuchLadder Exception is thrown when no word ladder is found between two words in the
 *         WordLadderSolver class
 */
public class NoSuchLadderException extends Exception
{
   private static final long serialVersionUID = 1L;

   public NoSuchLadderException(String message)
   {
      super(message);
   }

   public NoSuchLadderException(String message, Throwable throwable)
   {
      super(message, throwable);
   }
}
