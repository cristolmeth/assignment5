/*
Josh Cristol
Kenneth Lee
Michael Rust
EE422C - 16010
 */
package assignment5;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * @author Josh Cristol, Michael Rust, Kenneth Lee
 *         Finds a word ladder between two specified words
 *         The WordLadderSolver also accounts for invalid input words or no if no word ladder can be found
 */
public class WordLadderSolver implements Assignment5Interface
{
   private static Dictionary dictionary;

   private StopWatch watch;

   private HashSet<String> covered;

   private static final int NUMBER_LETTERS = 26;

   /**
    * creates a dictionary and Hashset covered which stores words that have already been covered
    * in the depth-first search
    * also creates a stopwatch object for performance analysis
    */
   public WordLadderSolver()
   {
      watch = new StopWatch();
      dictionary = new Dictionary();
      covered = new HashSet<String>();
   }

   /**
    * Determines if the specified word has been covered in the depth-first search yet
    *
    * @param target
    * @return true/false if the word has been covered/not covered
    */
   private boolean checkCovered(String target)
   {
      return covered.contains(target);
   }

   /**
    * Adds the specified word to the covered list
    *
    * @param target
    */
   private void addTempToCovered(String target)
   {
      covered.add(target);
   }

   @Override
   /**
    * Finds the word ladder between a given startword and endword
    * a word ladder is a list of valid english words with each subsequent element
    * separated by only one letter
    * @param startword
    * @param endword
    * @return the wordladdder as a list of strings
    */
   public List<String> computeLadder(String startWord, String endWord) throws NoSuchLadderException
   {
      watch.start();
      List<String> result = executeLadder(new StringBuilder(startWord), new StringBuilder(endWord), new ArrayList<String>());
      watch.stop();
      if (!validateResult(startWord, endWord, result))
         throw new NoSuchLadderException("No valid Ladder");
      return result;
   }

   /**
    * Recursive method to find a word ladder between a start word and end word
    *
    * @param startWord
    * @param endWord
    * @param ladder
    * @return List of strings containing a word ladder or empty
    */
   private List<String> executeLadder(StringBuilder startWord, StringBuilder endWord, List<String> ladder)
   {
      //Add the current startWord to the ladder which will eventually contain the complete word ladder
      ladder.add(startWord.toString());
      //Since the starWord has been covered, it is added to the covered list
      addTempToCovered(startWord.toString());
      //Two for loops iterate over every position in the five letter word and then every letter in the 
      //English alphabet to cover all possible words that could branch from the current word
      for (int i = 0; i < startWord.length(); i++)
      {
         for (int j = 0; j < NUMBER_LETTERS; j++)
         {
            char tchar = startWord.charAt(i);
            tchar -= 'a';
            tchar += 1;
            tchar %= 26;
            tchar += 'a';
            String tmp = "" + tchar;
            startWord = startWord.replace(i, i + 1, tmp);
            //Endword found, wordLadder is complete -> return up the call chain of recursive calls
            if (startWord.toString().equals(endWord.toString()))
            {
               ladder.add(startWord.toString());
               return ladder;
               //Make sure current word is valid and hasn't been covered
            } else if (dictionary.checkDictionary(startWord.toString()) && !checkCovered(startWord.toString()))
            {
               //Recursively call execute ladder to find the next word in the ladder
               ladder = executeLadder(startWord, endWord, ladder);
               //Word ladder found, return up the call chain
               if (ladder.get(ladder.size() - 1).equals(endWord.toString()))
               {
                  return ladder;
               }
            }
         }
      }
      //The current word is not part of the word ladder, remove it and return 
      ladder.remove(ladder.size() - 1);
      return ladder;
   }

   @Override
   /**
    * Determines if a given word ladder is valid-
    * a word ladder is a list of valid english words with each subsequent element
    * separated by only one letter
    * @param starWord
    * @param endWord
    * @wordLadder
    * return true/false if the word ladder is valid
    */
   public boolean validateResult(String startWord, String endWord, List<String> wordLadder)
   {
      if (wordLadder.size() <= 1)
      {
         return false;
      }
      return true;
   }

   /**
    * Returns the time it took to find a word ladder
    *
    * @return
    */
   public double getTime()
   {
      return watch.getElapsedTime();
   }
}
